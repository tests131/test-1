$(function () {
	var order_column_number = 2;
	var columns_definition 	= [];

	if (is_super_admin) {
		order_column_number = 3;
		columns_definition.push({ "className": "dt-nowrap", "sClass" : "text-nowrap", "data": "dataset_action", "name": "dataset_action"});
	}

	columns_definition.push({"className": "dt-nowrap", "sClass" : "text-nowrap", "data": "submitted_by", "name": "submitted_by"},
						    {"className": "dt-nowrap", "sClass" : "text-nowrap", "data": "location", "name": "location"},
							{"data": "subject", "name": "subject"},
						    {"className": "dt-nowrap", "sClass" : "text-nowrap", "data": "created_at", "name": "created_at"});

    $("#issue-form-dataset").DataTable({dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-5'i><'col-sm-7'p>>",
								    "columns": columns_definition,
									"iDisplayLength": 100,
						      		"lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
						      		bAutoWidth: true,
									"responsive": false,
									"processing": true,
									"serverSide": true,
						      		"serverMethod": "POST",
						      		"ajax": {url : project_url+"app/issue_dataset/list", type : "POST"},
						      		"order": [[ order_column_number, "DESC" ]],
									"paging": true,
									"searching": true,
									"scrollX": true,
									"scroller": true,
	});
});
