<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_issue_form_data extends MY_Model
{
	protected $_table = 'issue_form_data';

	protected $accessibleFields = ['user_id', 'office_id', 'reporting_week', 'subject', 'is_issue', 'description', 'created_at'];

	function getAllFormData($showDeleted = false)
	{
		$this->db->group_start();
		$this->db->where('deleted', $showDeleted)->or_where('deleted', false);
		$this->db->group_end();
		$this->db->where('is_issue', true);
		$this->db->order_by('created_at', 'DESC');
		return $this->db->get($this->_table);
	}

	function getFormDataCount($showDeleted, $keyword)
	{
		$this->db->join('users u', 'u.user_id = '.$this->_table.'.user_id');
		$this->db->join('offices o', 'o.office_id = '.$this->_table.'.office_id');

		$this->db->group_start();
		$this->db->where($this->_table.'.deleted', $showDeleted)->or_where($this->_table.'.deleted', false);
		$this->db->group_end();
		$this->db->where($this->_table.'.is_issue', true);

		if ($keyword)
		{
			$this->db->group_start();
			$this->db->like('u.title', $keyword);
			$this->db->or_like('u.first_name', $keyword);
			$this->db->or_like('u.last_name', $keyword);
			$this->db->or_like('o.office_name', $keyword);
			$this->db->or_like($this->_table.'.reporting_week::text', $keyword, 'both', false);
			$this->db->or_like($this->_table.'.subject::text', $keyword, 'both', false);
			$this->db->or_like($this->_table.'.created_at::text', $keyword, 'both', false);
			$this->db->group_end();
		}

		return $this->db->count_all_results($this->_table);
	}

	function getFormData($showDeleted = false, $start, $length, $order_by, $order, $keyword)
	{
		$this->db->select($this->_table.'.issue_form_data_id,
			CONCAT_WS(\' \', u.title, u.first_name, u.last_name) AS submitted_by,
			o.office_name AS location,
			'.$this->_table.'.subject,
			'.$this->_table.'.description,
			'.$this->_table.'.created_at');
		$this->db->join('users u', 'u.user_id = '.$this->_table.'.user_id');
		$this->db->join('offices o', 'o.office_id = '.$this->_table.'.office_id');

		if ($keyword)
		{
			$this->db->group_start();
			$this->db->like('u.title', $keyword);
			$this->db->or_like('u.first_name', $keyword);
			$this->db->or_like('u.last_name', $keyword);
			$this->db->or_like('o.office_name', $keyword);
			$this->db->or_like($this->_table.'.subject::text', $keyword, 'both', false);
			$this->db->or_like($this->_table.'.created_at::text', $keyword, 'both', false);
			$this->db->group_end();
		}

		$this->db->group_start();
		$this->db->where($this->_table.'.deleted', $showDeleted)->or_where($this->_table.'.deleted', false);
		$this->db->group_end();

		// this condition picks rows that are marked as "issue"
		$this->db->where($this->_table.'.is_issue', true);

		if ($length > 0)
     		$this->db->limit($length, $start);

	    $this->db->order_by($order_by, $order);

		return $this->db->get($this->_table);
	}

	function getFormDataById($showDeleted = false, $issue_form_data_id)
	{
		$this->db->select($this->_table.'.issue_form_data_id,
            '.$this->_table.'.user_id,
            '.$this->_table.'.office_id,
            o.office_name,
            '.$this->_table.'.reporting_week,
            '.$this->_table.'.subject,
			'.$this->_table.'.is_issue,
			'.$this->_table.'.description,
            '.$this->_table.'.created_at');
		$this->db->join('users u', 'u.user_id = '.$this->_table.'.user_id');
		$this->db->join('offices o', 'o.office_id = '.$this->_table.'.office_id');

		$this->db->where($this->_table.'.issue_form_data_id', $issue_form_data_id);

		$this->db->group_start();
		$this->db->where($this->_table.'.deleted', $showDeleted)->or_where($this->_table.'.deleted', false);
		$this->db->group_end();

		return $this->db->get($this->_table);
	}

	function insertFormData($data)
	{
		$newData = [
			'user_id' => $data['user_id'],
			'office_id' => $data['office_id'],
			'reporting_week' => $data['reporting_week'],
			'subject' => $data['subject'],
			'is_issue' => isset($data['is_issue']) ? true : false,
			'description' => $data['description']
		];

		$data = $this->permittedFields($newData, $this->accessibleFields);
		$data = $this->updateTimestamps($newData, 'insert');
		$this->writeDB->insert($this->_table, $newData);
		return (($this->writeDB->affected_rows() == 1) ? $this->writeDB->insert_id() : false );
	}

	function getAllFormsData($fields, $where)
	{
		return $this->db->select($fields)
						->join('users u', 'u.user_id=issue.user_id')
						->where($where)
						->get($this->_table.' issue');
	}

	function updateFormData($issue_form_data_id, $data)
	{
		$data = $this->permittedFields($data, $this->accessibleFields, 'update');
		$data = $this->updateTimestamps($data, 'update');

		$this->writeDB->where('issue_form_data_id', $issue_form_data_id)
					  ->set($data)
					  ->update($this->_table);

		if ($this->writeDB->affected_rows() == 1) {
			return true;
		} else {
			return false;
		}
	}

	public function deleteFormData($issue_form_data_id)
	{
		$data = ['deleted' => true];
		$data = $this->updateTimestamps($data, 'delete');

		$this->writeDB->where('issue_form_data_id', $issue_form_data_id)
			->set($data)
			->update($this->_table);

		if ($this->writeDB->affected_rows() >= 1) {
			return true;
		} else {
			return false;
		}
	}

	public function restoreFormData($issue_form_data_id)
	{
		$data = ['deleted' => false, 'deleted_at' => null];
		$data = $this->updateTimestamps($data, 'delete');

		$this->writeDB->where('issue_form_data_id', $issue_form_data_id)
					  ->set($data)
					  ->update($this->_table);

		if ($this->writeDB->affected_rows() >= 1) {
			return true;
		} else {
			return false;
		}
	}

	public function issuesChart($showDeleted)
	{
		$this->db->select("
			issue_form_data.office_id,
			issue_form_data.reporting_week,
			count(*) AS issues
		");

		$this->db->where($this->_table.'.reporting_week>=', $this->input->post('start_date'));
		$this->db->where($this->_table.'.reporting_week<=', $this->input->post('end_date'));
		$this->db->where($this->_table.'.is_issue=true');

		if (sizeof($this->input->post('offices')))
			$this->db->where_in($this->_table.'.office_id', $this->input->post('offices'));

		$this->db->group_start();
		$this->db->where($this->_table.'.deleted', $showDeleted)->or_where($this->_table.'.deleted', false);
		$this->db->group_end();

		$this->db->group_by('issue_form_data.office_id, issue_form_data.reporting_week');

		return $this->db->get($this->_table);
	}
}

/* End of file Model_issue_form_data.php */
