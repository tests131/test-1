<?php

use Phinx\Seed\AbstractSeed;
use Phinx\Util\Literal;

class LocationSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run()
    {
		// Truncate so this can be rebuilt in demo/dev environments
		$this->execute('TRUNCATE TABLE organizations RESTART IDENTITY CASCADE');
		$this->execute('TRUNCATE TABLE organizations_users RESTART IDENTITY CASCADE');
		$this->execute('TRUNCATE TABLE offices RESTART IDENTITY CASCADE');
		$this->execute('TRUNCATE TABLE offices_organizations RESTART IDENTITY CASCADE');
		$this->execute('TRUNCATE TABLE offices_users RESTART IDENTITY CASCADE');
		$this->execute('TRUNCATE TABLE issue_form_data RESTART IDENTITY CASCADE');
		// Truncate so this can be rebuilt in demo/dev environments

		$organizationsArray = [
			[
				'organization_id' => 1,
				'organization_name' => 'Vision Specialists',
				'created_at' => Literal::from('now()')
			]
		];

		$organizations = $this->table('organizations');
		$organizations->insert($organizationsArray)->saveData();

		$officesArray = [
			[
				'office_id' => 1,
				'office_name' => 'Clocktower Village',
				'created_at' => Literal::from('now()')
			],
			[
				'office_id' => 2,
				'office_name' => 'Council Bluffs',
				'created_at' => Literal::from('now()')
			],
			[
				'office_id' => 3,
				'office_name' => 'Papillion',
				'created_at' => Literal::from('now()')
			],
			[
				'office_id' => 4,
				'office_name' => 'NW Omaha (156th & W. Maple)',
				'created_at' => Literal::from('now()')
			],
			[
				'office_id' => 5,
				'office_name' => 'SW Omaha (180th & Harrison)',
				'created_at' => Literal::from('now()')
			]
		];

		$offices = $this->table('offices');
		$offices->insert($officesArray)->saveData();

		$officesOrganizationsArray = [
			['office_id' => 1, 'organization_id' => 1],
			['office_id' => 2, 'organization_id' => 1],
			['office_id' => 3, 'organization_id' => 1],
			['office_id' => 4, 'organization_id' => 1],
			['office_id' => 5, 'organization_id' => 1],
		];

		$offices_organizations = $this->table('offices_organizations');
		$offices_organizations->insert($officesOrganizationsArray)->saveData();

		$organizationsUsersArray = [
			['user_id' => 3, 'organization_id' => 1],
			['user_id' => 4, 'organization_id' => 1],
			['user_id' => 5, 'organization_id' => 1],
			['user_id' => 6, 'organization_id' => 1],
			['user_id' => 7, 'organization_id' => 1],
			['user_id' => 8, 'organization_id' => 1],
		];

		$organizations_users = $this->table('organizations_users');
		$organizations_users->insert($organizationsUsersArray)->saveData();

		$officesUsersArray = [
			['user_id' => 3, 'office_id' => 2],
			['user_id' => 4, 'office_id' => 1],
			['user_id' => 5, 'office_id' => 2],
			['user_id' => 5, 'office_id' => 4],
			['user_id' => 6, 'office_id' => 5],
			['user_id' => 6, 'office_id' => 2],
			['user_id' => 7, 'office_id' => 3],
			['user_id' => 8, 'office_id' => 2]
		];

		$offices_users = $this->table('offices_users');
		$offices_users->insert($officesUsersArray)->saveData();

		$issuesArray = [];
        for ($i=0; $i<20; $i++) {
            $subject = 'Subject '.$i;
			$isIssue = rand(0,1) === 1 ? true : false;

            array_push($issuesArray, [
                'user_id' => rand(3,8),
                'office_id' => rand(1,5),
                'reporting_week' => '2022-06-20',
                'subject' => 'Subject #'.$i,
				'is_issue' => $isIssue,
				'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. At risus viverra adipiscing at in tellus integer feugiat. Quis vel eros donec ac odio tempor. Mauris a diam maecenas sed enim ut sem viverra aliquet. Auctor elit sed vulputate mi sit. Vitae auctor eu augue ut lectus arcu bibendum at varius. Tempus iaculis urna id volutpat. Consectetur libero id faucibus nisl tincidunt eget nullam non. Aliquam purus sit amet luctus venenatis lectus magna fringilla urna. Id semper risus in hendrerit gravida rutrum quisque. Orci phasellus egestas tellus rutrum tellus pellentesque eu. Amet cursus sit amet dictum sit amet justo donec. Volutpat commodo sed egestas egestas fringilla phasellus faucibus scelerisque. Neque laoreet suspendisse interdum consectetur libero id faucibus nisl. Lorem dolor sed viverra ipsum. Bibendum arcu vitae elementum curabitur vitae. Ullamcorper velit sed ullamcorper morbi.'
            ]);
        }

		$issues_data = $this->table('issue_form_data');
		$issues_data->insert($issuesArray)->saveData();
    }
}
